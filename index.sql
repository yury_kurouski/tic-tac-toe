CREATE
    OR REPLACE FUNCTION NewGame() RETURNS VOID AS
$$
BEGIN

    DROP TABLE IF EXISTS GameState;

    CREATE
        TEMP TABLE GameState
    (
        X   INTEGER,
        Y   INTEGER,
        Val CHAR(1)
    );

    INSERT INTO GameState
        (X, Y, Val)
    SELECT x, y, NULL
    FROM generate_series(1, 3) AS x,
         generate_series(1, 3) AS y;

    RAISE
        NOTICE 'New game started. The board is empty.';
END;
$$
    LANGUAGE plpgsql;

CREATE
    OR REPLACE FUNCTION NextMove(px INTEGER, py INTEGER, pval CHAR(1) DEFAULT NULL)
    RETURNS TABLE
            (
                X   INTEGER,
                Y   INTEGER,
                Val CHAR(1)
            )
AS
$$
DECLARE
    current_val CHAR(1);
    next_val
                CHAR(1);
    x_count
                INTEGER;
    o_count
                INTEGER;
    found_winner
                CHAR(1);
BEGIN
    IF
        px < 1 OR px > 3 OR py < 1 OR py > 3 THEN
        RAISE EXCEPTION 'Invalid move: coordinates out of bounds';
    END
        IF;

    SELECT gs.Val
    INTO current_val
    FROM GameState gs
    WHERE gs.X = px
      AND gs.Y = py;
    IF
        current_val IS NOT NULL THEN
        RAISE EXCEPTION 'Invalid move: cell already occupied';
    END
        IF;

    IF
        pval IS NULL THEN
        SELECT COUNT(*)
        INTO x_count
        FROM GameState gs
        WHERE gs.Val = 'X';
        SELECT COUNT(*)
        INTO o_count
        FROM GameState gs
        WHERE gs.Val = 'O';
        IF
            x_count > o_count THEN
            next_val := 'O';
        ELSE
            next_val := 'X';
        END
            IF;
    ELSE
        next_val := pval;
    END
        IF;

    UPDATE GameState gs
    SET Val
            = next_val
    WHERE gs.X = px
      AND gs.Y = py;

    SELECT winner.Val
    INTO found_winner
    FROM (SELECT gs.Val
          FROM GameState gs
          WHERE gs.Val IS NOT NULL
          GROUP BY gs.Val, gs.X
          HAVING COUNT(*) = 3

          UNION ALL

          SELECT gs.Val
          FROM GameState gs
          WHERE gs.Val IS NOT NULL
          GROUP BY gs.Val, gs.Y
          HAVING COUNT(*) = 3

          UNION ALL

          SELECT gs.Val
          FROM GameState gs
          WHERE gs.Val IS NOT NULL
            AND gs.X = gs.Y
          GROUP BY gs.Val
          HAVING COUNT(*) = 3

          UNION ALL

          SELECT gs.Val
          FROM GameState gs
          WHERE gs.Val IS NOT NULL
            AND gs.X + gs.Y = 4
          GROUP BY gs.Val
          HAVING COUNT(*) = 3) AS winner
    LIMIT 1;

    IF
        found_winner IS NOT NULL THEN
        RAISE NOTICE 'Game over. Winner: %', found_winner;
    ELSE
        -- Check for a draw
        IF NOT EXISTS (SELECT 1
                       FROM GameState gs
                       WHERE gs.Val IS NULL) THEN
            RAISE NOTICE 'Game over. It''s a draw.';
        END
            IF;
    END
        IF;

    RETURN QUERY
        SELECT gs.X, gs.Y, gs.Val
        FROM GameState gs
        ORDER BY gs.X, gs.Y;
END;
$$
    LANGUAGE plpgsql;

SELECT NewGame();

SELECT *
FROM GameState
ORDER BY X, Y;

SELECT *
FROM NextMove(1, 1);
SELECT *
FROM NextMove(1, 2);
SELECT *
FROM NextMove(2, 2);

SELECT *
FROM GameState
ORDER BY X, Y;


